LIBVIRT_URI = "qemu:///system"

TEMPLATE_PATH = {
    "x86_64": "./templates/domain_x86.xml.jinja",
    "s390x": "./templates/domain_s390x.xml.jinja",
}

VMIMAGE = {
    "provider": "fedora",
    "version": "39",
    "checksum": "cd4200e908280ef993146a06769cf9ea8fda096a",
}

CACHE_DIR = "/tmp/dovetail-cache"
