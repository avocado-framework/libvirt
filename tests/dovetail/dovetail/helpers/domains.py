# Copyright (C) 2024 Red Hat, Inc.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see
# <http://www.gnu.org/licenses/>.


import os

import libvirt
from dovetail.exceptions import TestSetupException
from dovetail.utils import read_file

# pylint: disable=wrong-import-order
from jinja2 import Template
from lxml import objectify

# pylint: disable=ungrouped-imports
from dovetail import defaults


def from_xml_path(conn, xml_path):
    xml_content = read_file(xml_path)
    return conn.createXML(xml_content)


def from_xml_template(conn, prefix, arguments=None):
    arguments = arguments or {}

    if "arch" not in arguments:
        arguments["arch"] = os.uname()[4]

    template_path = defaults.TEMPLATE_PATH[arguments["arch"]]
    if not os.path.isfile(template_path):
        error = f"Template {template_path} not found."
        raise TestSetupException(error)

    # Adding a prefix to the name
    name = arguments.get("name", "dovetail-test")
    arguments["name"] = f"{prefix}-{name}"

    template = Template(read_file(template_path))
    xml_content = template.render(**arguments)
    return conn.createXML(xml_content)


def get_or_create_from_xml_path(conn, xml_path):
    xml_content = read_file(xml_path)
    root = objectify.fromstring(xml_content)
    try:
        name = root.name
    except AttributeError as exc:
        error = "Invalid XML, domain name not found."
        raise TestSetupException(error) from exc

    try:
        return conn.lookupByName(name.text)
    except libvirt.libvirtError:
        return conn.createXML(xml_content)


class DomainHandler:
    def __init__(self, conn, domain_id):
        self.conn = conn
        self.domain_id = domain_id
        self._domain = None

    def create_domain(self, arguments=None):
        """Creates a libvirt domain based on a generic template.

        This will receive some arguments that will be rendered on the
        template.  For more information about the arguments, see
        templates/domain.xml.jinja.

        If you are using this method from a test method (different from
        setUp()), AND you would like to count its call as a "setup/bootstrap"
        stage, consider using the following Avocado decorator:

        from avocado.core.decorators import cancel_on

        @cancel_on(TestSetupException)
        def test_foo(self):
          ...

        In that way, your test will not FAIL, instead it will be cancelled in
        case of any problem during this bootstrap.

        :param dict arguments: A key,value dictionary with the arguments
                               to be replaced on the template. If
                               any missing argument, template will be
                               rendered with default values.
        """
        try:
            self._domain = from_xml_template(self.conn, self.domain_id, arguments)
            return self._domain
        except (TestSetupException, OSError) as ex:
            msg = f"Failed to create domain: {ex}"
            raise TestSetupException(msg) from ex

    def destroy_domain(self):
        if self._domain:
            try:
                self._domain.destroy()
            except libvirt.libvirtError:
                pass
