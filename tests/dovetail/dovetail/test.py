# Copyright (C) 2024 Red Hat, Inc.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see
# <http://www.gnu.org/licenses/>.

"""Basic test helper module to avoid code redundancy."""

import os

import libvirt
import libvirtaio
from avocado import Test
from avocado.utils import vmimage
from dovetail.exceptions import LibvirtConnectionException
from dovetail.helpers.domains import DomainHandler

from dovetail import defaults


def connect(libvirt_uri):
    """Helper method for connecting to hypervisor.

    :param libvirt_uri: URI to hypervisor for example qemu:///system
    :raises LibvirtConnectionException: When the connection to hypervisor fails.
    """
    try:
        return libvirt.open(libvirt_uri)
    except libvirt.libvirtError as exc:
        msg = f"Failed to open connection with the hypervisor using {libvirt_uri}"
        raise LibvirtConnectionException(msg) from exc


class LibvirtTest(Test):
    """Main class helper for tests.

    Any test that inherits from this class, will have some methods and
    properties to assist on their jobs.
    """

    def setUp(self):
        """Setup to be executed before each test.

        Currently, this method is creating just a basic hypervisor connection.
        Please, extend this method when writing your tests for your own needs.

        Any error that happens here *will not* flag the test as "FAIL", instead
        tests will be flagged as "ERROR", meaning that some bootstrap error has
        happened.
        """
        self.image = None
        self.defaults = defaults
        self.events_impl = libvirtaio.virEventRegisterAsyncIOImpl()
        try:
            self.conn = connect(self.defaults.LIBVIRT_URI)
        except LibvirtConnectionException as exc:
            self.cancel(exc)
        self.domain_handler = DomainHandler(self.conn, f"{os.getpid()}-{self.id()}")

    def tearDown(self):
        """Shutdown after each test.

        This will destroy all previously created domains by this test, and
        remove any image snapshot if created.
        """
        self.domain_handler.destroy_domain()
        if isinstance(self.image, vmimage.Image):
            if os.path.exists(self.image.path):
                os.remove(self.image.path)
        self.conn.close()
