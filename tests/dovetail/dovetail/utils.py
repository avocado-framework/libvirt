# Copyright (C) 2024 Red Hat, Inc.
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library.  If not, see
# <http://www.gnu.org/licenses/>.

import os

from avocado.utils import vmimage
from dovetail.exceptions import TestSetupException


def read_file(filepath, encoding="utf-8"):
    with open(filepath, "r", encoding=encoding) as file_object:
        return file_object.read()


def get_generic_image(image, cache_dir):
    """Ask Avocado to fetch an VM image snapshot.

    Avocado will handle if image is already downloaded into the
    cache dir and also will make sure the checksum is matching.

    This will return an Image object pointing to a snapshot file. So
    multiple calls of this method will never return the same object.

    If you are using this method from a test method (different from
    setUp()), AND you would like to count its call as a "setup/bootstrap"
    stage, consider using the following Avocado decorator:

    from avocado.core.decorators import cancel_on

    @cancel_on(TestSetupException)
    def test_foo(self):
        ...

    In that way, your test will not FAIL, instead it will be cancelled in
    case of any problem during this bootstrap.

    :param image: Vmimage descriptino with data about `provider`, `version`
                  and `checksum`.
    :type image: dict
    :param cache_dir: Path to the test cache directory.
    :type cache_dir: str
    :raises TestSetupException: If the vmimage plugin wasn't able to provide
                                the image.
    """
    try:
        return vmimage.Image.from_parameters(
            name=image.get("provider"),
            version=image.get("version"),
            arch=image.get("arch", os.uname()[4]),
            cache_dir=cache_dir,
            checksum=image.get("checksum"),
        )
    except (OSError, AttributeError) as ex:
        msg = f"Failed to get a generic image: {ex}"
        raise TestSetupException(msg) from ex
